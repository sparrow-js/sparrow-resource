// 绑定类别的事件
$("#.chkIsAlbumId")
    .bind(
    "onclick",
    function (e) {
        if (this.checked == true) {
            var queryString = "forumCode="
                + $("hdnForumCode").value;
            ajax
                .json($.url.root
                + "/cms/album-list.json?"
                + queryString,null,
                function (result) {
                    if ($("#.sltAlbum").addJson(result.message, "cmsId", "title")) {
                        $("sltAlbum").style.visibility = "visible";
                    }
                    else {
                        $.alert(
                            "该版块下无任何专辑，请先新建专辑!",
                            "sad");
                    }
                });
        } else {
            $("#.sltAlbum").removeAll();
            $("sltAlbum").style.visibility = "hidden";
        }
    });