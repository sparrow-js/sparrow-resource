﻿var cmsEditor = new SparrowEditor("cmsEditor");
var forumTree =null;
// 文章提交按钮
function submit_click(e, isNext, isPage) {
    // 是否只新建内容页(newPage.jsp按钮)
    // 根据内容页UUID判断是否更新该页内容.
    cmsEditor.config.attach.actionUrl =$("hdnCmsId").value==""?"new.do":"update.do";
    // 自动根据hdnCmsId进行判断
    // 调用 编辑器提交事件
    cmsEditor.attach.submit();
}

// 编辑器内容加载
document
    .ready(function () {
        //先配置再初始化
        cmsEditor.config.tool.height =65;
        cmsEditor.config.tool.adjust.Adjustable = true;
        cmsEditor.config.tool.convertHTML.isConvert = true;
        // 设置附件的父控件
        cmsEditor.attach.setParentObject(cmsEditor);
        cmsEditor.config.attach.fileInfoId = "hdnFileInfo";
        // cmsEditor.config.attach.key=$("hdnPathKey").value;
        // 编辑器上传的图片全部为cms作为key
        cmsEditor.config.attach.key = "cms";
        // 不显示图片信息
        cmsEditor.config.attach.showImageInfo = false;
        cmsEditor.attach.validate = function () {
            $("hdnContent").value = cmsEditor.getEditorContent();
            v.isNull(cmsInfor.txtContent);
            return v.getValidateResult(cmsInfor, false);
        };
        cmsEditor.initialize("divEditor");
        cmsEditor.initImageUploadEvent();

        forumTree = new iTree("forumTree");
        forumTree.config.floatTreeId = "divForumList";
        forumTree.config.descHiddenId = "hdnForumCode";
        forumTree.config.descTextBoxId = "txtForumName";
        forumTree.initForum("loadOption=brothers|"
        + $("hdnForumCode").value);
        $.include($("chkIsAlbumId"), "/javascript/system/cms/album.js");
    });