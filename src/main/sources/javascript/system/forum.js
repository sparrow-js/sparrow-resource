﻿//版块管理的tree对象
var iForum = new iTree("iForum");
// 父版块选择对话
var iSelectForum = new iTree("iSelectForum");
// 版块的重建函数辅助版块选择对话框
iForum.config.reBuildTree = function () {
    iSelectForum.resetIcon();
    for (var i = 0; i < iForum.aNodes.length; i++) {
        var t = iForum.aNodes[i].remark ? iForum.aNodes[i].remark.ForumType : 1;
        iSelectForum.addremark(iForum.aNodes[i].id, iForum.aNodes[i].pid,
            iForum.aNodes[i].name, "javascript:nodeClick();",
            getForumType(t), iForum.aNodes[i].remark);
    }
    var selectTree = $("new.div." + iForum.config.floatTreeId);
    selectTree.s.innerHTML = iSelectForum;
    document.body.appendChild(selectTree.s);
};
// 版块管理加载函数
document.ready(function () {
    ajax.json($.url.root + "/forum/load.json",null, function (result) {
        iForum.aNodes = [];
        iForum.config.showOrder = true;
        iForum.config.orderURL = $.url.root + "/forum/order.json";
        iForum.config.treeFrameId = "divForumTree";
        iForum.config.floatTreeId = "forumSelectTree";
        iForum.config.showOrder = false;
        iForum.delete_click = delete_click;
        iForum.resetIcon();
        iForum.add(0, -1, browser.getCookie(browser.cookie.website_name),
            "javascript:nodeClick(0);");
        var jsonList = result.message;
        for (var i = 0; i < jsonList.length; i++) {
            iForum.addremark(jsonList[i].uuid, jsonList[i].parentUUID,
                jsonList[i].code + "|" + jsonList[i].name,
                "javascript:nodeClick();", jsonList[i].code + ":"
                + getForumType(jsonList[i].forumType),
                jsonList[i]);
        }
        $("divForumTree").innerHTML = iForum;
        resetForm();
    });
});
// 重置版块输入项
function resetForm() {
    $.clearForm(forumInfo);
    var cn = iForum.aNodes[iForum.getSelectedAi()];
    if (cn) {
        $("txtParentForumName").value = cn.name;
        $("hdnParentUUID").value = cn.id;
        if (cn.remark) {
            $("txtForumCode").value = cn.remark.code;
        } else {
            $("txtForumCode").value = "";
        }
    }
}
// 版块点击事件
function nodeClick() {
    if (iForum.floatTreeFrameId) {
        var cn = iSelectForum.aNodes[iSelectForum.getSelectedAi()];
        $("txtParentForumName").value = cn.name;
        $("hdnParentUUID").value = cn.id;
        var currentNodeIndex = (cn.childCount + 1) + "";
        if (cn.remark) {
            $("txtForumCode").value = cn.remark.code
            + currentNodeIndex.leftAlignWithChar();
        } else {
            $("txtForumCode").value = currentNodeIndex.leftAlignWithChar();
        }
        iForum.clearFloatFrame();
        v.isNull(forumInfo.txtParentForumName);
    } else {
        var cn = iForum.aNodes[iForum.getSelectedAi()];
        if (cn) {
            if (cn.pid == -1) {
                $("btnInsertForum").value = l.command.save;
                $("editStatus").innerHTML = l.command.save;
                $("btnUpdateForum").value = l.command.cancel;
                resetForm();
            } else {
                $("txtParentForumName").value = $.toString(cn._parentNode.name);
                $("hdnParentUUID").value = cn._parentNode.id;
                $("txtForumCode").value = cn.remark.code;
                $("txtForumType").value = $.toString(cn.remark.forumType, 2);
                $("txtMaxRecordCount").value = $.toString(cn.remark.maxRecordCount);
                $("txtOpenType").value = $.toString(cn.remark.openType, 'manage');
                $("imgForumIco").innerHTML = "<img style='border:0px;' src='"
                + $.toString(cn.remark.forumIcoUrl, $.defaultForumIcoUrl) + "'/>";
                $("imgForumCover").innerHTML = "<img style='border:0px;' src='"
                + $.toString(cn.remark.cover, $.defaultForumIcoUrl) + "'/>";

                $("hdnIco").value=$.toString(cn.remark.forumIcoUrl, $.defaultForumIcoUrl);
                $("hdnCover").value=$.toString(cn.remark.cover, $.defaultForumIcoUrl);

                $("txtManager").value = $.toString(cn.remark.manager,forumInfo.txtManager.defaultValue);
                $("txtRemark").value = $.toString(cn.remark.remark);
                $("spanTxtCount").innerHTML = 500 - $.toString(cn.remark.remark).getByteLength();
                $("txtManageUrl").value = $.toString(cn.remark.manageUrl,forumInfo.txtManageUrl.defaultValue);
                $("txtListUrl").value = $.toString(cn.remark.listUrl,forumInfo.txtListUrl.defaultValue);
                $("txtDetailUrl").value = $.toString(cn.remark.detailUrl,forumInfo.txtDetailUrl.defaultValue);
                $("txtAccessUrl").value = $.toString(cn.remark.accessUrl);
                $("txtNewUrl").value = $.toString(cn.remark.newUrl,forumInfo.txtNewUrl.defaultValue);
                $("txtUploadKey").value = $.toString(cn.remark.uploadKey);
                $("txtStatus").value = $.toString(cn.remark.status, 1);
                $("txtForumName").value = $.toString(cn.remark.name);
                $("btnUpdateForum").value = l.command.update;
                $("btnInsertForum").value = l.command.add;
                $("editStatus").innerHTML = l.command.update;
            }
        } else {
            resetForm();
        }
    }
}
// 新建或更新版块信息
function save(flag) {
    var actionUrl = $.url.root + (flag == l.command.update ? "/forum/update.json" : "/forum/new.json");
    var data= $.getFormData(forumInfo);
    if (flag == l.command.update) {
        data += "&mforum.uuid=" + iForum.getSelectedId();
    }
    ajax.json(actionUrl,data, function (result) {
        var forum=result.value;
            var newNode = new Node(forum.uuid, forum.parentUUID,
                forum.code + "|" + forum.name, "javascript:nodeClick();",
                forum.code + ":" + getForumType(forum.type), undefined,
                undefined, undefined,forum);
            if (flag == l.command.save) {
                iForum.AppendNode(newNode);
                $.message(l.message.save.replace("$forumName", forum.name));
            } else {
                iForum.UpdateNode(newNode);
                $.message(l.message.update.replace("$forumName", forum.name));
            }
    });
}
// 版块删除事件
function delete_click(ai) {
    var cn;
    if (!ai) {
        cn = iForum.aNodes[iForum.getSelectedAi()];
    } else {
        cn = iForum.aNodes[ai];
    }
    if (cn._hasChild) {
        $.message(l.message.delete_has_child);
    } else {
        if (window
                .confirm(l.message.delete_confirm)) {
            var deletePostString = "uuid=" + cn.id;
            ajax
                .json($.url.root + "/forum/delete.json",deletePostString,
                function (json) {
                        iForum.DeleteNode(cn);
                        resetForm();
                        $.message(l.message.del);
                });
        }
    }
}

iTree.prototype.dbs = function (nodeId) {
    var cn = iForum.aNodes[nodeId];
    var listUrl = $.toString(cn.remark.ListUrl);
    if (listUrl != "") {
        listUrl = $.url.root + listUrl + "?forumCode=" + cn.remark.Code;
        alert(listUrl);
    }
    else {
        $.message(l.message.list_url_not_set, this);
    }
};

file.validateUploadFile = function (f, key) {
    if (file.checkFileType(file.getFileName(f.value), ["jpg",
            "jpeg", "gif", "png"], "errorImgForumIco")) {
        file.uploadCallBack = function (fileInfo, clientFileName) {
            if (fileInfo.fileName) {
                if (key == "forum") {
                    $("imgForumIco").innerHTML = "<img src='" + fileInfo.fileName + "'/>";
                    $("hdnIco").value = fileInfo.fileName;
                } else if (key == "forum_cover") {
                    $("hdnCover").value = fileInfo.fileName;
                    $("imgForumCover").innerHTML = "<img src='" + fileInfo.fileName + "'/>";
                }
            }
        };
        file.uploadClick(false, "", key);
    }
};
// 更新事件
function command_click(obj) {
    if (obj.value == l.command.cancel) {
        nodeClick();
    }
    else if (obj.value == l.command.add) {
        obj.value = l.command.save;
        $("editStatus").innerHTML = l.command.save;
        $("btnUpdateForum").value = l.command.cancel;
        resetForm();
    }
    else {
        v.getValidateResult(forumInfo, function () {
            save(obj.value);
        });
    }
}
document.onclick = function () {
    iForum.clearFloatFrame();
    iSelectForum.clearFloatFrame();
};