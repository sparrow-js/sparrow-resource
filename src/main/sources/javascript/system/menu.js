﻿//该js文件取消 为节省js链接数
l = {
    command: {
        openAll: "展开全部",
        closeAll: "关闭全部"
    },
    message: {
        accessDenied: "访问被拒绝"
    }
};
ajax.referWindow = window.parent;
var menuTree = new iTree('menuTree');
function showMenu(result) {
    var json=result.message;
    if (json.length == 0) {
        $("divMenuTree").className = "highlight";
        $("divMenuTree").innerHTML = l.message.accessDenied;
    } else {
        menuTree.aNodes = [];
        menuTree.config.showOrder = false;
        menuTree.config.useIcons = true;
        menuTree.config.useLines = false;
        menuTree.config.useRootIcon = false;
        menuTree.config.usePlusMinusIcons = false;
        menuTree.config.useTreeIdInNodeClass = true;
        menuTree.config.useLevelInNodeClass = true;
        menuTree.config.closeSameLevel = true;
        menuTree.resetIcon();
        menuTree.add(0, -1, "");
        var menuList = json;
        var Url = "";
        for (var i = 0; i < menuList.length; i++) {
            if (menuList[i].manageUrl && menuList[i].forumType != 1) {
                Url = $.url.root
                + menuList[i].manageUrl.replace("$url",
                    menuList[i].accessUrl).replace("$forum",
                    menuList[i].code);
            } else {
                Url = "";
            }
            if (menuList[i].forumIcoUrl == "null"
                || menuList[i].forumIcoUrl == "") {
                menuList[i].forumIcoUrl = $.defaultForumIcoUrl;
            }
            menuTree.add(menuList[i].uuid, menuList[i].parentUUID,
                menuList[i].name, Url, menuList[i].name,
                menuList[i].openType, undefined, true, undefined,
                menuList[i].forumIcoUrl);
        }
        $("divMenuTree").innerHTML = menuTree;
        menuTree.openTo(menuTree.aNodes[1].id);
    }
}
document.ready(function () {
    $("divMenuTree").innerHTML = menuTree.config.loadingString;
    ajax.json($.url.root + "/loadPrivilege.json", null, showMenu);
    $("btnOpen").value = l.command.openAll;
});