var groupType = null;
document.ready(function() {
	groupType = new iTree("groupType");
	groupType.config.floatTreeId = "groupTypeTree";
	groupType.config.descTextBoxId = "txtGroupTypeName";
	groupType.config.descHiddenId="hdnGroupLevel";
	groupType.config.isAllowNull=false;
	groupType.config.validateFunction=function(){
		v.isNull(groupInfo.txtGroupType);
	};
	groupType.codeNodeCallBack = function(icodeEntity) {
			$("hdnGroupType").value = icodeEntity.remark.code;
	};
	groupType.initCodeToolip('GROUP-');
});
file.validateUploadFile = function(obj) {
	file.checkFileType(file.getFileName(obj.value),["jpg", "jpeg",
			"gif", "png"], "errorImgGroupIco");
};
function submit_click() {
		file.uploadCallBack = function(fileInfo) {
			var action = "new.do";
			if ($("hdnGroupId").value.trim()!='') {
				action = "update.do";
				$("hdnGroupNewIco").value = fileInfo.FileName;
			} else {
				$("hdnGroupIco").value = fileInfo.FileName;
			}
			$.submit(action);
		};
		file.uploadClick(false, $("hdnGroupIco").value);
}