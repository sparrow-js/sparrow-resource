﻿//用户组树
var iGroupTree = null;
// 资源树
var iResourceTree = new iTree('iResourceTree');
// 加载角色树
document.ready(function () {
    iGroupTree = new iTree("iGroupTree");
    iGroupTree.config.floatTreeId = "divRoleTree";
    iGroupTree.config.useIcons = true;
    iGroupTree.config.useLines = false;
    iGroupTree.config.useRootIcon = false;
    iGroupTree.config.usePlusMinusIcons = false;
    iGroupTree.config.useTreeIdInNodeClass = true;
    iGroupTree.config.useLevelInNodeClass = true;
    iGroupTree.resetIcon();
    // 延迟加载 鼠标点击时加载
    iGroupTree.config.loadFloatTree = function () {
        ajax
            .json($.url.root + "/group/load.json", null, loadGroupBackUp);
    };

    if ($.request("groupId")) {
        $("hdnGroupId").value = $.request("groupId");
        ajax.json($.url.root + "/group/load.json", null, loadGroupBackUp);
    }
});
// 角色、用户组回调
function loadGroupBackUp(result) {
    iGroupTree.aNodes = [];
    var jsonList = result;
    var groupTypeList = jsonList.groupType;
    var groupList = jsonList.groupList;
    iGroupTree.add(groupTypeList[0].parentUUID, -1, "");
    for (var i = 0; i < groupTypeList.length; i++) {
        // (id, pid, name, url, title, target,
        // childCount,showCtrl,remark,icon)
        iGroupTree.add(groupTypeList[i].code,
            groupTypeList[i].parentUUID, groupTypeList[i].name, "",
            groupTypeList[i].name, undefined, undefined, undefined,
            groupTypeList[i].code, iGroupTree.config.imageDir
            + "/base.gif");
    }

    for (i = 0; i < groupList.length; i++) {
        iGroupTree.add(groupList[i].groupId, groupList[i].groupType,
            groupList[i].groupName, "javascript:nodeClick();",
            groupList[i].groupName, undefined, undefined,
            undefined, groupList[i].groupType,
            iGroupTree.config.imageDir + "/base.gif");
    }

    $(iGroupTree.config.floatTreeId).innerHTML = iGroupTree;
    if ($.request("groupId")) {
        nodeClick(iGroupTree.aNodes[iGroupTree.getAiById($.request("groupId"))]);
    }
}

// 角色 点击事件
function nodeClick(cn) {
    if (!cn) {
        cn = iGroupTree.aNodes[iGroupTree.getSelectedAi()];
    }
    $("txtGroupName").value = cn.name;
    $("hdnGroupId").value = cn.id;
    $("divForumTreeTitle").innerHTML = l.message.resourceName;
    ajax.json($.url.root + "/group/initPrivilege.json", "groupId=" + cn.id, initPrivilege);
    iGroupTree.clearFloatFrame();
}
function initPrivilege(result) {
    writeResource(result.allResource,
        result.selectedResource);
    writeStrategy(result.allStrategy,result.selectedStrategy);
}

function checkResource(currentResource,selectedResource){
    var manageUrl="";
    if(currentResource.manageUrl){
        manageUrl=currentResource.manageUrl;
    }
    for (var j = 0; j < selectedResource.length; j++) {
        if (manageUrl.indexOf(selectedResource[j].resourceStrategy)==0) {
            var forumCode = selectedResource[j].value.trim();
            // 不限制forumCode
            if (forumCode == "true") {
                return currentResource.uuid;
            }
            else {
                var forumType = currentResource.forumType;
                var currentForumCode = currentResource.code;
                if (forumCode == currentForumCode) {
                    return currentResource.uuid;
                }
            }
        }
    }
}

function writeResource(allResource, selectedResource) {
    if (allResource !=null&&allResource.length>0) {
            iResourceTree.aNodes = [];
            iResourceTree.config.useIcons = true;
            iResourceTree.config.useLines = false;
            iResourceTree.config.useRootIcon = false;
            iResourceTree.config.usePlusMinusIcons = false;
            iResourceTree.config.useTreeIdInNodeClass = true;
            iResourceTree.config.useLevelInNodeClass = true;
            iResourceTree.config.useCheckbox = true;
            iResourceTree.resetIcon();
            iResourceTree.add(0, -1, "");
            var selectedResourceId=[];
            // (id, pid, name, url, title, target,
            // childCount,showCtrl,remark,icon)
            // title=manageURL+code
            for (var i = 0; i < allResource.length; i++) {
                iResourceTree.add(allResource[i].uuid,
                    allResource[i].parentUUID,
                    getForumType(allResource[i].forumType) + ":"
                    + allResource[i].name, "javascript:void(0)",
                    allResource[i].manageUrl + ": " + allResource[i].code,
                    undefined, undefined, true, allResource[i].code,
                    iResourceTree.config.imageDir + "/base.gif");
                selectedResourceId.push(checkResource(allResource[i],selectedResource));
            }
            $("divResource").innerHTML = iResourceTree;
            iResourceTree.openAll();
            iResourceTree.setChecked(selectedResourceId);
    }
}
function writeStrategy(allStrategy, selectedStrategy) {
    var strategyHTML = [];
    var selectedStrategyId = [];
    for (var i = 0; i < selectedStrategy.length; i++) {
        selectedStrategyId.push(selectedStrategy[i].resourceStrategy);
    }
    strategyHTML.push("<table style=\"width:100%;\" class='frame'>");
    for (var i = 0; i < allStrategy.length; i++) {
        if (allStrategy[i].value.trim() == "Type") {
            strategyHTML
                .push('<tr><th colspan="2" style="text-align:left;border-bottom:#ca151d 1px dotted;font-size:12pt;line-height:30px;color:#ff9900;">'
                + allStrategy[i].name + '</th></tr>');
        } else {
            strategyHTML.push('<tr><th>' + allStrategy[i].name + '</th>');
            if (allStrategy[i].Value.trim() == "CheckBox") {
                strategyHTML.push('<td><input id="' + allStrategy[i].code
                + '" name="StrategyCode" value="true" type="checkbox"');
                if (selectedStrategyId.indexOf(allStrategy[i].code) >= 0) {
                    strategyHTML.push(' checked="true"');
                }
                strategyHTML.push(' />');
            } else {
                strategyHTML.push('<td><input id="' + allStrategy[i].code
                + '" name="strategyCode" type="text" ');
                var index = selectedStrategyId.indexOf(allStrategy[i].code);
                if (index != -1) {
                    strategyHTML.push(' value="' + selectedStrategy[index].value
                    + '"');
                }
                strategyHTML.push(' />' + allStrategy[i].value);
            }
            strategyHTML.push('</td></tr>');
        }
    }
    strategyHTML.push("</table>");
    $("divStrategy").innerHTML = strategyHTML.join("");
}
document.onclick = function () {
    iGroupTree.clearFloatFrame();
};
function submit_click() {
    var strategyCodeArray = $("name.strategyCode");
    var selectedStrategy = [];
    for (var i = 0; i < strategyCodeArray.length; i++) {
        if (strategyCodeArray[i].type == "text") {
            selectedStrategy.push(strategyCodeArray[i].id + ": "
            + strategyCodeArray[i].value);
        } else if (strategyCodeArray[i].checked) {
            selectedStrategy.push(strategyCodeArray[i].id + ": true");
        }
    }
    var strPost = "selectedStrategy=" + selectedStrategy.join()
        + "&selectedResource=" + iResourceTree.getAllCheckedTitle()
        + "&groupId=" + $("hdnGroupId").value;
    ajax.json($.url.root + "/group/setPrivilege.json", strPost, function (result) {
        $.alert(groupInfo.setPrivilegeSucessMessage.replace("{0}",
            $("txtGroupName").value), "smile");
    });
}