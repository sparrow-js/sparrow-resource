var iCode = new iTree('iCode');
var iSelectingCode = null;
iCode.config.reBuildTree = function () {
    iSelectingCode = new iTree('iSelectingCode');
    iSelectingCode.resetIcon();
    var cn = iCode.aNodes[iCode.getSelectedAi()];
    for (var i = 0; i < iCode.aNodes.length; i++) {
        if (cn) {
            if (cn.pid == -1 || iCode.aNodes[i].id != cn.id
                && iCode.aNodes[i].pid != cn.id) {
                iSelectingCode.addremark(iCode.aNodes[i].id,
                    iCode.aNodes[i].pid, iCode.aNodes[i].name,
                    "javascript:nodeClick();", iCode.aNodes[i].title,
                    iCode.aNodes[i].value);
            }
        } else {
            iSelectingCode.addremark(iCode.aNodes[i].id, iCode.aNodes[i].pid,
                iCode.aNodes[i].name, "javascript:nodeClick();",
                iCode.aNodes[i].title, iCode.aNodes[i].value);
        }
    }
    var selectingTree = $("new.div." + iCode.config.floatTreeId);
    selectingTree.s.innerHTML = iSelectingCode;
    document.body.appendChild(selectingTree.s);
};
document.ready(function () {
    $("divCodeTree").innerHTML = iCode.config.loadingString;
    ajax.json($.url.root + "/code/load.json", "loadOption=all", loadCodeBackUp);
    document.onclick = function () {
        iCode.clearFloatFrame();
    };
});

function loadCodeBackUp(result) {
    iCode.aNodes = [];
    iCode.config.orderURL = $.url.root + "/system/code/order.do";
    iCode.config.delete_click = delete_click;
    iCode.config.treeFrameId = "divCodeTree";
    iCode.deleteItem = delete_click;
    iCode.config.showOrder = false;
    iCode.config.floatTreeId = "selectCodeTree";

    iCode.resetIcon();
    iCode.add(0, -1, "系统编码", "javascript:nodeClick(0);");

    var jsonList = result.message;
    for (var i = 0; i < jsonList.length; i++) {
        iCode.addremark(jsonList[i].uuid, jsonList[i].parentUUID,
            jsonList[i].name, "javascript:nodeClick();",
            jsonList[i].code, jsonList[i]);
    }
    $("divCodeTree").innerHTML = iCode;
    resetCode();
}
function resetCode() {
    $.clearForm(codeInfo);
    var cn = iCode.aNodes[iCode.getSelectedAi()];
    if (cn) {
        $("txtParentName").value = cn.name;
        $("hdnParentUUID").value = cn.id;
        if (cn.remark) {
            $("txtCode").value = cn.remark.code;
        } else {
            $("txtCode").value = "";
        }
    }
}
function nodeClick() {
    if (iCode.floatTreeFrameId) {
        var cn = iSelectingCode.aNodes[iSelectingCode.getSelectedAi()];
        $("txtParentName").value = cn.name;
        $("hdnParentUUID").value = cn.id;
        var currentNodeIndex = (cn.childCount + 1) + "";
        if (cn.remark) {
            $("txtCode").value = cn.remark.code
            + currentNodeIndex.leftAlignWithChar();
        } else {
            $("txtCode").value = currentNodeIndex.leftAlignWithChar();
        }
        iCode.clearFloatFrame();
        v.isNull(codeInfo.txtParentName);
    } else {
        var cn = iCode.aNodes[iCode.getSelectedAi()];
        if (cn) {
            if (cn.pid == -1) {
                $("btnInsertCode").value = l.command.save;
                $("editStatus").innerHTML = l.command.save;
                $("btnUpdateCode").value = l.command.cancel;
                resetCode();
            } else {
                var remark = "";
                if (cn.remark.Remark) {
                    remark = cn.remark.Remark;
                }
                $("txtParentName").value = cn._parentNode.name;
                $("hdnParentUUID").value = cn._parentNode.id;
                $("txtCode").value = cn.remark.code;
                $("txtName").value = cn.name;
                $("txtStatus").value = cn.remark.status;
                $("txtOrderNo").value = cn.remark.orderNo;
                $("txtValue").value = $.toString(cn.remark.value);
                $("txtRemark").value = $.toString(cn.remark.remark);
                $("spanTxtCount").innerHTML = 500 - remark
                    .getByteLength();
                $("btnUpdateCode").value = l.command.update;
                $("btnInsertCode").value = l.command.add;
                $("editStatus").innerHTML = l.command.update;
            }
        } else {
            resetCode();
        }
    }
}
function save(flag) {
    var actionUrl = $.url.root + (flag == l.command.update ? "/code/update.json" : "/code/new.json");
    var data = $.getFormData(codeInfo);
    if (flag == l.command.update) {
        data += "&mcode.uuid=" + iCode.getSelectedId();
    }
    ajax.json(actionUrl, data, function (result) {
        var json=result.message;
        var newNode = new Node(json.uuid, json.parentUUID,
            json.name, "javascript:nodeClick();", undefined, undefined,
            undefined, undefined, json);
        if (flag == l.command.save) {
            iCode.AppendNode(newNode);
            $.message(json.name + "”新建成功");
        } else {
            iCode.UpdateNode(newNode);
            $.message(json.name + "”更新成功");
        }
    });
}
function delete_click(ai) {
    var cn;
    if (!ai) {
        cn = iCode.aNodes[iCode.getSelectedAi()];
    } else {
        cn = iCode.aNodes[ai];
    }
    if (cn._hasChild) {
        $.message(l.message.delete_hash_child.replace("$name", cn.name));
    } else {
        if (window.confirm(l.message.delete_confirm.replace("$name", cn.name))) {
            var data = "uuid=" + cn.id;
            ajax.json($.url.root + "/code/delete.json", data, function (json) {
                iCode.DeleteNode(cn);
                resetCode();
                $.message(l.message.del);
            });
        }
    }
}

function command_click(obj) {
    if (obj.value == l.command.cancel) {
        nodeClick();
    }
    else if (obj.value == l.command.add) {
        obj.value = l.command.save;
        $("editStatus").innerHTML = l.command.save;
        $("btnUpdateCode").value = l.command.cancel;
        resetCode();
    }
    else {
        v.getValidateResult(codeInfo, function () {
            save(obj.value);
        });
    }
}

function getArea(code, areaCtrlArray) {
    if (!code || code.substring(4) != '00')return;
    ajax.req("POST", $.url.root + "/code/loadArea.do", function (xmlHttpRequest) {
        if (xmlHttpRequest.responseText != "") {
            var ctrl = null;
            if (!code)ctrl = areaCtrlArray[0];
            else if (code.indexOf('0000') == 2)ctrl = areaCtrlArray[1];
            else if (code.indexOf('00') == 4)ctrl = areaCtrlArray[2];
            jsonList = xmlHttpRequest.responseText.json();
            for (var i = 0; i < jsonList.length; i++) {
                $(ctrl).add(jsonList[i].code, jsonList[i].name);
            }
            var firstArea = xmlHttpRequest.responseText.json()[0].code;
            getArea(firstArea, areaCtrlArray);
        }
    }, true, "code=" + code);
}