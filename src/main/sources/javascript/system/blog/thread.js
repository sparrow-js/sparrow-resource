﻿/*后台主题管理*/
gridView.id = "grvThreadList";
/* 选择结果框 */
gridView.resultCtrlId = "hdnThreadId";
// 帖子的版块转移事件
function moveThread(e) {
	if (gridView.action("return")) {
		forumTree.Show(e, 200, 300);
	}
}
// 初始化主题转换版块树的加载方法
forumTree.config.loadFloatTree = function() {
	ajax.req("POST", rootPath + "/forum/load.do", loadForumCallBack, true,
			"loadOption=childByCode|001");
};
// 实现主题转换树的选择确认事件
function ok(forumCode) {
	var cn = forumTree.aNodes[forumTree.getSelectedAi()];
	if (window
			.confirm("\u60a8\u786e\u8ba4\u5c06\u9009\u4e2d\u7684\u4e3b\u9898\u79fb\u81f3\u300a"
					+ cn.name + "\u300b\u7248\u5757\u5417?")) {
		ajax.req("POST", rootPath + "/bbs/thread/changeForum.do", function(
				xmlHttpRequest) {
			if (xmlHttpRequest.responseText != "") {
				if (xmlHttpRequest.responseText.indexOf("|") != -1) {
					alert(xmlHttpRequest.responseText.split("|")[1]);
				} else {
					jalert(
							"\u6240\u9009\u95ee\u9898\u5df2\u8f6c\u81f3\u7248\u5757\u300a"
									+ cn.name + "\u300b", "smile");
					win.config.jalert.closeCallBack = function() {
						window.location.href = window.location.href;
					};
				}
			}
		}, true, "forumCode=" + forumCode + "&threadId="
				+ $(gridView.resultCtrlId).value);
	}
}
// 后台管理编辑主帖
function innerEditTitle(uuid, currentUserId) {
	hasEditPrivilege(uuid, true, currentUserId, "inneredit");
}

document.onclick = function() {
	forumTree.clearFloatFrame();
};