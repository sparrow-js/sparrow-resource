﻿var threadEditor = new SparrowEditor("threadEditor");

var editorTree = new iTree("editorTree");
editorTree.config.floatTreeId = "divWriter";
editorTree.config.descHiddenId = "hdnCreateUserId";
editorTree.config.descTextBoxId = "txtCreateUserName";
document.onclick = function () {
    editorTree.clearFloatFrame();
};

document.ready(function () {
    win.config.box.descContainer = $("divTag");
    win.config.isInFrame = false;
    threadEditor.config.tool.adjust.adjustable = false;
    threadEditor.config.tool.convertHTML.isConvert = true;
    threadEditor.initialize("divEditor");
    threadEditor.attach.setParentObject(threadEditor);
    //手动设置附件内容ID
    threadEditor.config.attach.fileInfoId = "hdnFileInfo";
    threadEditor.attach.validate = function () {
        $("hdnContent").value = threadEditor.getEditorContent();
        $("hdnTags").value = $checkBoxValue("select_tag").join(",");
        var errorMessage =[tempthreadInfo.txtTitle,tempthreadInfo.hdnContent];
        if (v.getValidateResult(errorMessage)) {
            return true;
        }
    };
    editorTree.initCodeToolip('EDITOR-');
});

function publish_click() {
    $("btnSubmit").disabled = true;
    threadEditor.attach.submit();
}
function noPass_click() {
    $("hdnParameter").value = "no_pass";
    $("btnNoPass").disabled = true;
    $.submit();
}
function delete_click() {
    $("hdnParameter").value = "delete";
    $("btnDelete").disabled = true;
    $.submit();
}
function changeImage(index, imageUrl) {
    imageUrl = encodeURIComponent(imageUrl);
    var url = rootPath + "/system/queue/changeImage.jsp?parameter=" + imageUrl + "&index=" + index;
    jwindow(500, 400, "查看原图", url);
}
function changeImageCallBack(srcElement, index) {
    //ajax请求后台
    var newUrl = encodeURIComponent(srcElement.src);
    ajax.req("GET", rootPath + "/system/queue/changeImage.do?parameter=" + newUrl + "&t=" + Math.random(), function (xmlHttpRequest) {
        threadEditor.$("tempImage" + index).src = xmlHttpRequest.responseText;
        win.closeClick();
    });
}