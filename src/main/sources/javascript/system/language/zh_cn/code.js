var l = {
	command: {
		save: "保存",
		cancel: "取消",
		update: "更新",
		add: "新建"
	},
	message: {
		save: "编码“$name”新建成功",
		update: "编码“$name”更新成功",
		delete_hash_child:"该编码下有子编码，故不能删除！",
		delete_confirm:"编码删除后将无法恢复，您确认将该编码删除？",
		del:"删除成功！",
		list_url_not_set:"未设置列表链接",
		error : "网络繁忙，请稍侯再试。",
		accessDenied:"访问被拒绝"
	}
};
var codeInfo = {
	txtParentName : {
		ctrlId : "hdnParentUUID",
		errorCtrlId : "errorParentName",
		prompt : "上级编码",
		nullError : "请选择上级编码"
	},
	txtCode : {
		ctrlId : "txtCode",
		errorCtrlId : "errorCode",
		prompt : "必须添写编码<br/>且必须继承至上级编码",
		nullError : "请输入编码"
	},
	txtOrderNo : {
		ctrlId : "txtOrderNo",
		errorCtrlId : "errorOrderNo",
		prompt : "请输入排序号",
		nullError : "请输入排序号"
	},
	txtName : {
		ctrlId : "txtName",
		errorCtrlId : "errorName",
		prompt : "名称为必填项",
		nullError : "请输入名称"
	},
	txtStatus : {
		ctrlId : "txtStatus",
		errorCtrlId : "errorStatus",
		prompt : "0无效 1 有效",
		options :[0, 1],
		defaultValue : 1
	},
	txtValue : {
		ctrlId : "txtValue",
		errorCtrlId : "errorValue",
		prompt : "请输入该配置项的值",
		allowNull:true
	},
	txtRemark : {
		ctrlId : "txtRemark",
		errorCtrlId : "errorRemark",
		prompt : "请设置该项配置说明。",
		allowNull:true
	}
};