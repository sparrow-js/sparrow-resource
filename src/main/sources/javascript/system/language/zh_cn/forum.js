var l = {
    command: {
        save: "保存",
        cancel: "取消",
        update: "更新",
        add: "新建"
    },
    message: {
        save: "版块“$forumName”新建成功",
        update: "版块“$forumName”更新成功",
        delete_hash_child: "该版块下有子版块，故不能删除！",
        delete_confirm: "版块删除后将无法恢复，您确认将该版块删除？",
        del: "删除成功！",
        list_url_not_set: "未设置列表链接",
        accessDenied:"访问被拒绝"
    }
};
var forumInfo =
{
    txtParentForumName: {
        ctrlId: "hdnParentUUID",
        errorCtrlId: "errorParentForumName",
        prompt: "新版块的所属版块",
        nullError: "请选择新版块的所属版块"
    },
    txtForumCode: {
        ctrlId: "txtForumCode",
        errorCtrlId: "errorForumCode",
        prompt: "版块编码要继承自上级版块编码",
        nullError: "版块编码不能为空，且要继承自上级版块编码"
    },
    txtForumName: {
        ctrlId: "txtForumName",
        errorCtrlId: "errorForumName",
        prompt: "版块名称",
        nullError: "版块名称不能为空"
    },
    txtAccessUrl: {
        ctrlId: "txtAccessUrl",
        errorCtrlId: "errorAccessUrl",
        prompt: "请输入访问该版块的url路径<br/>例如:JAVA版块的url为java",
        allowNull:true
    },
    txtForumType: {
        ctrlId: "txtForumType",
        errorCtrlId: "errorForumType",
        prompt: "1系统菜单 2系统页面 3事件",
        options:[1, 2, 3],
        defaultValue:2
    },
    txtMaxRecordCount: {
        ctrlId: "txtMaxRecordCount",
        errorCtrlId: "errorMaxRecordCount",
        prompt: "版块下允许新建的最大记录数<br/>-1为不限制(默认)",
        defaultValue: -1
    },
    txtManager: {
        ctrlId: "txtManager",
        errorCtrlId: "errorManager",
        prompt: "版块的管理员 默认为admin",
        defaultValue:"admin"
    },
    txtManageUrl: {
        ctrlId: "txtManageUrl",
        errorCtrlId: "errorManageUrl",
        prompt: "版块后台管理地址",
        defaultValue:"/cms/manage.jsp?forumCode=$forum"
    },
    txtNewUrl: {
        ctrlId: "txtNewUrl",
        errorCtrlId: "errorNewUrl",
        prompt: "版块发帖时的地址",
        defaultValue:"/cms/new.jsp"
    },
    txtListUrl: {
        ctrlId: "txtListUrl",
        errorCtrlId: "errorListUrl",
        prompt: "版块列表显示地址",
        defaultValue:"/list"
    },
    txtDetailUrl: {
        ctrlId: "txtDetailUrl",
        errorCtrlId: "errorDetailUrl",
        prompt: "版块下内容显示地址",
        defaultValue:"/template/detail.jsp"
    },
    txtUploadKey: {
        ctrlId: "txtUploadKey",
        errorCtrlId: "errorUploadKey",
        prompt: "请输入文件上传的AttachKey.<br/>与system_config.properties中配置的一致。",
        allowNull:true
    },
    txtOpenType: {
        ctrlId: "txtOpenType",
        errorCtrlId: "errorOpenType",
        prompt: "打开方式 _blank _self _parent manage 默认为manage",
        option:["_blank","_self","_parent","manage"],
        defaultValue:"manage"
    },
    txtStatus: {
        ctrlId: "txtStatus",
        errorCtrlId: "errorStatus",
        prompt: "0无效 1 有效 2 只有admin有管理权限",
        options: [0, 1, 2, 3],
        defaultValue:1
    },
    hdnCover: {
        ctrlId: "hdnCover",
        allowNull:true
    },
    hdnIco: {
        ctrlId: "hdnIco",
        allowNull:true
    },
    txtRemark: {
        ctrlId: "txtRemark",
        errorCtrlId: "errorRemark",
        prompt: "版块简介，不超过500 字",
        allowNull:true
    }
};