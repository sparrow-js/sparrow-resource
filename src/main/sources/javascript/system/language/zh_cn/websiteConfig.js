websiteConfigInfo = {
	txtTitle : {
		ctrlId : 'txtTitle',
		errorCtrlId : 'errorTitle',
		prompt : '请输入title[标题]',
		nullError : 'title[标题]不允许为空'
	},
	txtKeywords : {
		ctrlId : 'txtKeywords',
		errorCtrlId : 'errorKeywords',
		prompt : '请输入keywords[关键字]',
		nullError : 'keywords[关键字]不允许为空'
	},
	txtDescription : {
		ctrlId : 'txtDescription',
		errorCtrlId : 'errorDescription',
		prompt : '请输入description[描述]',
		nullError : 'description[描述]不允许为空'
	},
	flbLogo : {
		ctrlId : 'flbLogo',
		errorCtrlId : 'errorLogo',
		prompt : '请输入logo',
		nullError : 'logo不允许为空'
	},
	flbBanner : {
		ctrlId : 'flbBanner',
		errorCtrlId : 'errorBanner',
		prompt : '请输入banner',
		nullError : 'banner不允许为空'
	},
	txtIcp : {
		ctrlId : 'txtIcp',
		errorCtrlId : 'errorIcp',
		prompt : '请输入icp备案信息',
		nullError : 'icp备案信息不允许为空'
	},
	txtContact : {
		ctrlId : 'txtContact',
		errorCtrlId : 'errorContact',
		prompt : '请输入联系我们',
		nullError : '联系我们不允许为空'
	},
	flbBannerFlash : {
		ctrlId : 'flbBannerFlash',
		errorCtrlId : 'errorBannerFlash',
		prompt : '请输入bannerFlash',
		nullError : 'bannerFlash不允许为空'
	}
};