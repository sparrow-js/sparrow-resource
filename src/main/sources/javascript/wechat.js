function initConfig(appId, timestamp, noceStr, signature) {
    var config = {
        appId: appId, // 必填，公众号的唯一标识
        timestamp: timestamp, // 必填，生成签名的时间戳
        nonceStr: noceStr, // 必填，生成签名的随机串
        signature: signature,// 必填，签名，见附录1
        jsApiList: [
            'checkJsApi',
            'onMenuShareTimeline',
            'onMenuShareAppMessage',
            'onMenuShareQQ',
            'onMenuShareWeibo',
            'onMenuShareQZone',
            'hideMenuItems',
            'showMenuItems',
            'hideAllNonBaseMenuItem',
            'showAllNonBaseMenuItem',
            'translateVoice',
            'startRecord',
            'stopRecord',
            'onVoiceRecordEnd',
            'playVoice',
            'onVoicePlayEnd',
            'pauseVoice',
            'stopVoice',
            'uploadVoice',
            'downloadVoice',
            'chooseImage',
            'previewImage',
            'uploadImage',
            'downloadImage',
            'getNetworkType',
            'openLocation',
            'getLocation',
            'hideOptionMenu',
            'showOptionMenu',
            'closeWindow',
            'scanQRCode',
            'chooseWXPay',
            'openProductSpecificView',
            'addCard',
            'chooseCard',
            'openCard'
        ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
    };
    wx.config(config);
};

function initShareConfig(sharConfig) {
    wx.onMenuShareAppMessage(sharConfig);
    wx.onMenuShareTimeline(sharConfig);
    wx.onMenuShareQQ(sharConfig);
    wx.onMenuShareWeibo(sharConfig);
    wx.onMenuShareQZone(sharConfig);
}
wx.ready(function () {
    var share_config = {
        title: "我在Dunkin' Donuts 首吃趴赢了大奖,快来看看吧",
        desc: "Dunkin' Donuts 首吃趴 赢全年免费咖啡",
        link: '${root_path}/lucky_draw_share.jsp',
        imgUrl: '${root_path}/images/share.jpg'
    };
    initConfig
    initShareConfig(share_config);
    
});