﻿Sparrow.register = {
    validate: {
        email: function (obj) {
            if (v.isEmail(regInfo.txtEmail) == true) {
                ajax.json($.url.root + "/user/validate-email.json", "email=" + $("txtEmail").value, function (result) {
                    if (result.message == false) {
                        v.ok(regInfo.txtEmail);
                        regInfo.txtEmail.isExist = false;
                    } else {
                        regInfo.txtEmail.isExist = true;
                        v.fail(regInfo.txtEmail);
                    }
                });
            }
        },
        user: function () {
            if (v.isNull(regInfo.txtUserName) == true) {
                if (v.isUserNameRule(regInfo.txtUserName)) {
                    ajax.json($.url.root + "/user/validate-user-name.json", "userLoginName=" + $("txtRegUserName").value, function (result) {
                        if (result.message == false) {
                            v.ok(regInfo.txtUserName);
                            regInfo.txtUserName.isExist = false;
                        } else {
                            regInfo.txtUserName.isExist = true;
                            v.fail(regInfo.txtUserName);
                        }
                    });
                }
            }
        }
    },
    /* 注册事件 */
    shortcut: function () {
        if (v.getValidateResult(regInfo, false)) {
            ajax
                .json(
                $.url.root + "/shortcut/register.do",
                $.getFormData(regInfo),
                function (result) {
                    /* 回调委托名称 */
                    var callBackParameter = browser.request("callBackParameter");
                    /* 注册成功后默认登录状态 */
                    browser.setCookie(
                        browser.cookie.permission,
                        result.permission, 0, browser.cookie.root_domain);
                    parent.loginCallBack(callBackParameter);
                });
        } else {
            return false;
        }
    }
};


Sparrow.login = {
    /* 快捷登录事件 */
    shortcut: function () {
        /* 验证数组 */
        if (v.getValidateResult(loginInfo, false)) {
            var loginDays = $("checkedValue.loginDays").s;
            var postString = $.getFormData(loginInfo);
            if (loginDays) {
                postString += "&loginDays=" + loginDays;
            }
            ajax
                .json(
                $.url.root + "/shortcut/login.json",
                postString,
                function (result) {
                    /* 密码保存天数 */
                    var days = $("rdb15").checked ? parseInt($("rdb15").value) * 24 * 60 * 60
                        : 0;
                    browser.setCookie(browser.cookie.permission,
                        result.permission, days, browser.cookie.root_domain);
                    /* 操作选项 */
                    var option = $.request("option");
                    /* 回调参数 */
                    var callBackParameter = $
                        .request("parameter");
                    /* 注册成功后默认登录状态 */
                    //Sparrow.comment.callback(args);
                    eval("parent.$." + option + ".login_callback('" + callBackParameter + "')");
                    parent.win.closeClick();
                });
        } else {
            return false;
        }
    },
    init: function () {
        var tabs = $("#.tabs");
        v.background_color = false;
        if (tabs != null && tabs.s != null) {
            var config = {};
            if ($.request("shortRegister") == "true") {
                config = {
                    index: 1
                };
                $("txtEmail").focus();
            } else {
                $("txtUserName").focus();
            }
            $("#.tabs").tabs(config);
        }
    }
};
document.ready(function () {
    $.login.init();
    $(document).enter(function(){
        var submitButton=$("divShortCutRegister").className=="block"?$("btnRegister"):$("btnLogin");
        submitButton.onclick();
    });
});
