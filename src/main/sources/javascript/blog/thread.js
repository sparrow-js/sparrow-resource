﻿var commentEditor = new SparrowEditor("commentEditor");
document.ready(function () {
    $.user.initLoginBar();
    v.background_color=false;
    commentEditor.config.titleCtrlId = "txtReturnTitle";
    commentEditor.config.tool.adjust.adjustable = false;
    commentEditor.config.tool.convertHTML.isConvert = false;
    commentEditor.config.style = "comment";
    commentEditor.config.tool.height =38;
    commentEditor.initialize("divEditor");

    // 手动设置附件内容ID
    commentEditor.config.attach.fileInfoId = "hdnFileInfo";
    commentEditor.config.attach.key = "comment";
    commentEditor.config.contentCtrlId = "hdnCommentContent";
    commentEditor.attach.validate = function () {
        if (!browser.isLogin()) {
            $.user.login.dialog($.user.login.option.comment);
            return false;
        } else {
            return v.getValidateResult(commentInfo, false);
        }
    };
    // 调用 编辑器提交事件
    commentEditor.attach._submit = function () {
        var data =["hdnThreadId","hdnCommentId","hdnFileInfo","hdnCommentContent"];
        ajax.req("POST", $.url.root
        + "/blog/thread/comment.do", function (httpRequest) {
            if (httpRequest.responseText.indexOf(ajax.FAIL) > -1) {
                $.message(httpRequest.responseText.trim().split("|")[1]);
            }
            else {
                commentEditor.setEditorContent("");
                $("divCommentList").innerHTML = httpRequest.responseText.trim();
            }
        }, true, $.getFormData(data));
    };
    commentEditor.attach.setParentObject(commentEditor);
    $.d3Share.init();
    $.thread.count.init();

    window.onscroll = $.floating.init;

    //初始化主帖的附件
    $.attach.config.attachJson = $("thread_attach_json").innerHTML.trim()
        .json();
    $.attach.config.contentHtml = $("thread_content").innerHTML;
    $.attach.config.descContainerId = "thread_attach";
    $.attach.init();

    // 所有帖子ID
    var commentIdArray = $("name.commentId");
    for (var i = 0; i < commentIdArray.length; i++) {
        $.attach.config.attachJson = $("comment_attach_json_"
        + commentIdArray[i].value).innerText.json();
        $.attach.config.contentHtml = $("comment_content_"
        + commentIdArray[i].value).innerHTML;
        $.attach.config.descContainerId = "comment_attach_"
        + commentIdArray[i].value;
        $.attach.init();
    }

    file.validateUploadFile = function (e) {
        if (file.checkFileType(e.value, [".jpg", '.png', '.gif'])) {
            file.multiFile = -1;
            file.uploadClick(true, '', commentEditor.config.attach.iframeId
                .format("image"), commentEditor, e);
        }
    };
    // 鼠标离开头象效果
    document.onmouseover = function (e) {
        $("#.divUserInfo").hidden();
        var forum = $("name.forum");
        if (forum != null) {
            for (var i = 0; i < forum.length; i++) {
                $("div" + forum[i].value).style.display = "none";
            }
        }
    };
    Sparrow.thread.comment.login_callback=function(){
        commentEditor.attach.submit();
    }
});
