document.ready(function() {
	$.d3Share.init();
	$.user.initLoginBar();
	$.thread.count.init();
	window.onscroll = $.floating.init;
	document.onmouseover = function(e) {
		$("#.divUserInfo").hidden();
	};
});